package com.example.frvalent.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

public class DialOutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dial_out);
        this.setWindowParams();

        Intent intent = getIntent();
        String phoneNumber = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        TextView numberView=(TextView) findViewById(R.id.numberTest);
        numberView.setText(phoneNumber);


    }

    public void setWindowParams() {
        WindowManager.LayoutParams wlp = getWindow().getAttributes();
        wlp.dimAmount = 0;
        wlp.flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        wlp.x = -20;
        wlp.height = 700;
        wlp.width = 900;
        wlp.y = -10;
        getWindow().setAttributes(wlp);

    }
}
