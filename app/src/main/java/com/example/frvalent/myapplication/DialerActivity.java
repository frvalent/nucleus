package com.example.frvalent.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.unboundid.asn1.ASN1OctetString;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import static com.example.frvalent.myapplication.R.id.destNumber;
import static com.example.frvalent.myapplication.R.id.myGridView;
import static com.example.frvalent.myapplication.R.id.parent;



public class DialerActivity extends AppCompatActivity {

    public boolean physicsMode = false;

    public boolean getPhysicsMode() {
        return this.physicsMode;

    }

    ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialer);


        Intent intent = getIntent();
        String phoneNumber = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        final TextView numberView = (TextView) findViewById(R.id.destNumber);
        numberView.setText(phoneNumber);

        //triggers LDAP search
        numberView.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        Pattern fixedRegex = Pattern.compile("^(0041|0)+(227)+([67]\\d+)$");
                        Pattern mobileRegex = Pattern.compile("^(0041|0)+(75411|16)+(\\d+)$");
                        boolean mFixed = fixedRegex.matches(fixedRegex,s.toString());
                        boolean mFixed = mobileRegex.matches(fixedRegex,s.toString());

                        if (s.length()>=5 && s.toString().matches(fixed_regex)){
                            numberView.append(getCallerInfo("foo"));
                        }

                    }
                }


        );


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Ringing...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final GridView gridView = (GridView) findViewById(R.id.myGridView);
        // NUCLEUS FAB INIT

        FloatingActionButton nucleusfab = (FloatingActionButton) findViewById(R.id.nucleusFab);
        nucleusfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                physicsMode = !physicsMode;
                String modeText = physicsMode ? "on" : "off";

                gridView.setAdapter(new ImageAdapter(view.getContext(), physicsMode));
                Snackbar.make(view, "Physics Mode is " + modeText + "!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //getCallerInfo("foo");
                numberView.append(getCallerInfo("foo"));


            }
        });

        //GRIDVIEW INIT


        gridView.setAdapter(new ImageAdapter(this, false));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                /*Toast.makeText(DialerActivity.this, "" + position,
                        Toast.LENGTH_SHORT).show();*/

                TextView destNumber = (TextView) findViewById(R.id.destNumber);

                if (position < 9) {
                    int realpos = position + 1;
                    destNumber.append("" + realpos);
                } else if (position == 10) {
                    destNumber.append("0");
                } else if (position == 9) {
                    destNumber.append("*");
                } else if (position == 11) {
                    destNumber.append("#");
                }
            }
        });


    }

    private LDAPConnection getLdapConnection() {
        try {
            //URI ldapUrl = new URI("ldap://xldap.cern.ch");
            //String bindUserName=settings.getString(Keys.realm.ldap.username,"");
            //String bindPassword=settings.getString(Keys.realm.ldap.password,"");
            //int ldapPort = ldapUrl.getPort();

            //if (ldapPort == -1) ldapPort = 389;
            //LDAPConnection conn = new LDAPConnection(ldapUrl.getHost(), ldapPort, null, null);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy); // this avoid networking calls from the main thread
            LDAPConnection conn = new LDAPConnection();
            conn.connect("xldap.cern.ch",389);

            return conn;

        } catch (LDAPException e) {
            e.printStackTrace();
        }
        return null;
    }


    private String getCallerInfo(String destNumber) {

        LDAPConnection conn = this.getLdapConnection();
        SearchResult result = null;

        try {
            SearchRequest request = new SearchRequest("OU=Users,OU=Organic Units,DC=cern,DC=ch",
                    SearchScope.SUB, Filter.create("(&(objectClass=user)(telephoneNumber=+41227662549))"));
            result = conn.search(request);

        } catch (LDAPException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.close();
            }

            SearchResultEntry entry = result.getSearchEntries().get(0);
            String dept = entry.getAttributeValue("description");
            String fullname = entry.getAttributeValue("displayName");

            System.out.println("Search result" + fullname + dept);
            return fullname + "\n" +dept;

        }


    }
}