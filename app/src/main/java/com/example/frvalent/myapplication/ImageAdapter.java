package com.example.frvalent.myapplication;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by Curro on 17/05/2017.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private boolean physicsMode;

    public ImageAdapter(Context c, boolean pMode) {
        mContext = c;
        physicsMode = pMode;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {


        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            //imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
            //imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //imageView.setPadding(1, 1, 1, 1);
        } else {
            imageView = (ImageView) convertView;
        }

        if (physicsMode) {
            imageView.setImageResource(mThumbIds[position]);
        }
        else {
            imageView.setImageResource(mNumIds[position]);
        }
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.hydrogen, R.drawable.helium, R.drawable.lithium,
            R.drawable.beryllium, R.drawable.boron, R.drawable.carbon,
            R.drawable.nitrogen, R.drawable.oxygen, R.drawable.flourine,
            R.drawable.star, R.drawable.neutron, R.drawable.hash
    };

    private Integer[] mNumIds = {
            R.drawable.uno, R.drawable.dos, R.drawable.tres,
            R.drawable.cuatro, R.drawable.cinco, R.drawable.seis,
            R.drawable.siete, R.drawable.ocho, R.drawable.nueve,
            R.drawable.asterisk, R.drawable.cero, R.drawable.pound
    };

}