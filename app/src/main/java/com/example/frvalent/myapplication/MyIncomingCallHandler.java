package com.example.frvalent.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

/**
 * Created by Curro on 09/05/2017.
 */



public class MyIncomingCallHandler extends BroadcastReceiver{

    Boolean isTest = false; // stops hijacking the dialer
    public static final String EXTRA_MESSAGE  = "com.example.myfirstapp.MESSAGE";


    @Override
    public void onReceive(Context context, Intent intent) {
        // Extract phone number reformatted by previous receivers

        String ringing = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        String phoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
        System.out.println("Inside the Incoming Broadcast Receiver with state: " +ringing + " and incoming number:" +phoneNumber);

        if (TelephonyManager.EXTRA_STATE_RINGING.equals(ringing)) {

            String broadcastreturn = isTest?phoneNumber:null;

            setResultData(broadcastreturn);
            // Start my DIALER activity

            intent.putExtra(EXTRA_MESSAGE, phoneNumber);
            intent.setClassName("com.example.frvalent.myapplication", "com.example.frvalent.myapplication.DialerActivity");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);



        } else {

        }



    }


}
