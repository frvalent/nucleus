package com.example.frvalent.myapplication;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;
import java.util.List;

public class DisplayCellsActivity extends AppCompatActivity {

    TextView textView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_cells);




        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        //TextView textView = (TextView) findViewById(R.id.textView);
        //textView.setText(message);


        TelephonyManager tm=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        textView1=(TextView) findViewById(R.id.textView);
        textView1.setMovementMethod(new ScrollingMovementMethod());
        String IMEINumber=tm.getDeviceId();
        String subscriberID=tm.getSubscriberId();
        String myPhoneNumber = tm.getLine1Number();
        if (myPhoneNumber.equals("") ){
            myPhoneNumber = "Unavailable";
        }
        String SIMSerialNumber=tm.getSimSerialNumber();
        String networkCountryISO=tm.getNetworkCountryIso().toUpperCase();
        String SIMCountryISO=tm.getSimCountryIso().toUpperCase();
        //String softwareVersion=tm.getDeviceSoftwareVersion();
        //String voiceMailNumber=tm.getVoiceMailNumber();
        String networkOperator=tm.getNetworkOperator();
        String networkOperatorName=tm.getNetworkOperatorName();

        List<CellInfo> allCells;
        try {
            allCells = tm.getAllCellInfo();
        } catch (NoSuchMethodError e) {
            allCells = null;

        }
        if ((allCells == null) || allCells.isEmpty()) {


        }

        String strphoneType="";

        int phoneType=tm.getPhoneType();

        switch (phoneType)
        {
            case (TelephonyManager.PHONE_TYPE_CDMA):
                strphoneType="CDMA";
                break;
            case (TelephonyManager.PHONE_TYPE_GSM):
                strphoneType="GSM";
                break;
            case (TelephonyManager.PHONE_TYPE_NONE):
                strphoneType="NONE";
                break;
        }

        //getting information if phone is in roaming
        boolean isRoaming=tm.isNetworkRoaming();

        String info="Phone Number: " + myPhoneNumber + "\n";
        info+="\n IMEI Number: "+IMEINumber;
        info+="\n SubscriberID: "+subscriberID;
        info+="\n Sim Serial Number: "+SIMSerialNumber;
        info+="\n Network Country: "+networkCountryISO;
        info+=" SIM Country: "+SIMCountryISO;
        //info+="\n Software Version: "+softwareVersion;
        //info+="\n Voice Mail Number: "+voiceMailNumber;
        //info+="\n Phone Network Type: "+strphoneType;
        info+="\n In Roaming?: "+isRoaming;
        info+="\n MCC: "+networkOperator.substring(0,3) + " MNC: "+ networkOperator.substring(4,5) + " Name: "+ networkOperatorName;

        //BEGIN OF CELL INFO

        List<Location> rslt = new ArrayList<Location>();
        int i =1;
        for (android.telephony.CellInfo inputCellInfo : allCells) {
            Location cellLocation = null;
            if (inputCellInfo instanceof CellInfoGsm) {
                CellInfoGsm gsm = (CellInfoGsm) inputCellInfo;
                CellIdentityGsm id = gsm.getCellIdentity();
                String registered = gsm.isRegistered()?"REGISTERED":"UNREGISTERED";;
                CellSignalStrengthGsm sig = gsm.getCellSignalStrength();
                //cellLocation = db.query(id.getMcc(), id.getMnc(), id.getCid(), id.getLac());
                info+="\n GSM CELL"+ i ;
                info+="\n " + registered;
                info+="\n        MCC:" +id.getMcc()+"   MNC: "+id.getMnc()+"  CID"+id.getCid()+"  LAC"+id.getLac();
                info+="\n        " + -sig.getDbm()/10+" dBm " + sig.getAsuLevel() + "asu (Level "+ sig.getLevel()+"/4)";

            } else if (inputCellInfo instanceof CellInfoWcdma) {

                CellInfoWcdma wcdma = (CellInfoWcdma) inputCellInfo;
                CellIdentityWcdma id = wcdma.getCellIdentity();
                String registered = wcdma.isRegistered()?"REGISTERED":"UNREGISTERED";;
                CellSignalStrengthWcdma sig = wcdma.getCellSignalStrength();

                //cellLocation = db.query(id.getMcc(), id.getMnc(), id.getCid(), id.getLac());
                info+="\n WCDMA CELL "+ i ;
                info+="\n " + registered;
                info+="\n        MCC:" +id.getMcc()+"   MNC: "+id.getMnc()+"  CID"+id.getCid()+"  LAC"+id.getLac();
                info+="\n        " + -sig.getDbm()/10+" dBm " + sig.getAsuLevel() + "asu (Level "+ sig.getLevel()+"/4)";


            }
            else if (inputCellInfo instanceof CellInfoLte) {

                CellInfoLte lte = (CellInfoLte) inputCellInfo;
                String registered = lte.isRegistered()?"REGISTERED":"UNREGISTERED";

                CellIdentityLte id = lte.getCellIdentity();
                CellSignalStrengthLte sig = lte.getCellSignalStrength();
                //cellLocation = db.query(id.getMcc(), id.getMnc(), id.getCid(), id.getLac());
                //info += "\n LTE CELL  :" + id.getMcc() + " " + id.getMnc() + " " + id.getCi() + " " + id.getTac();
                info+="\n LTE CELL "+ i ;
                info+="\n " + registered;
                info+="\n        MCC:" +id.getMcc()+"   MNC: "+id.getMnc()+"  CI: "+id.getCi()+"  TAC: "+id.getTac()  + "PCI " + id.getPci() ;
                info+="\n        " + -sig.getDbm()/10+" dBm " + sig.getAsuLevel() + "asu (Level "+ sig.getLevel()+"/4)";

            }

            else {

                info+="\n UNIDENTIFIED CELL TYPE: "+ i ;
            }


            if ((cellLocation != null)) {
                rslt.add(cellLocation);

            }

            i++;
        }


        ///END OF CELL INFO









        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location locationGPS = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);

        info+="\n\n\n GPS - Latitude: "+locationGPS.getLatitude() +" Longitude: "+locationGPS.getLongitude()+" Altitude: "+locationGPS.getAltitude();

        textView1.setText(info);//displaying the information in the textView


        //MapView mapView = (MapView) findViewById(R.id.mapView);
        //mapView.setBuiltInZoomControls(true);

        //GoogleMap map;
        //map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView)).getMap;
        //map.setMyLocationEnabled(true);

        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showSubFloating(v);
            }
        });

        FloatingActionButton mySubFab1 = (FloatingActionButton) findViewById(R.id.subFloating1);
        mySubFab1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createTicket(v);
            }
        });

        FloatingActionButton mySubFab2 = (FloatingActionButton) findViewById(R.id.subFloating2);
        mySubFab2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createTicket(v);
            }
        });

        FloatingActionButton mySubFab3 = (FloatingActionButton) findViewById(R.id.subFloating3);
        mySubFab3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createTicket(v);
            }
        });

        FloatingActionButton mySubFab4 = (FloatingActionButton) findViewById(R.id.subFloating4);
        mySubFab4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createTicket(v);
            }
        });


    }

    public void showSubFloating(View view){

        FloatingActionButton subFloating1 = (FloatingActionButton) findViewById(R.id.subFloating1);
        FloatingActionButton subFloating2 = (FloatingActionButton) findViewById(R.id.subFloating2);
        FloatingActionButton subFloating3 = (FloatingActionButton) findViewById(R.id.subFloating3);
        FloatingActionButton subFloating4 = (FloatingActionButton) findViewById(R.id.subFloating4);

        if (subFloating1.getVisibility()==View.VISIBLE){
            subFloating1.setVisibility(View.INVISIBLE);

        }

        else if (subFloating1.getVisibility()==View.INVISIBLE){
            subFloating1.setVisibility(View.VISIBLE);
        }


        if (subFloating2.getVisibility()==View.VISIBLE){
            subFloating2.setVisibility(View.INVISIBLE);
        }

        else if (subFloating2.getVisibility()==View.INVISIBLE){
            subFloating2.setVisibility(View.VISIBLE);
        }

        if (subFloating3.getVisibility()==View.VISIBLE){
            subFloating3.setVisibility(View.INVISIBLE);
        }

        else if (subFloating3.getVisibility()==View.INVISIBLE){
            subFloating3.setVisibility(View.VISIBLE);
        }

        if (subFloating4.getVisibility()==View.VISIBLE){
            subFloating4.setVisibility(View.INVISIBLE);
        }

        else if (subFloating4.getVisibility()==View.INVISIBLE){
            subFloating4.setVisibility(View.VISIBLE);
        }



    }


    public void createTicket(View view){ //from the tutorial


        textView1=(TextView) findViewById(R.id.textView);

        CharSequence bodyText = textView1.getText();
        String subject = "Mobile network measures ticket";

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","frvalent@cern.ch", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);


        emailIntent.putExtra(Intent.EXTRA_TEXT, bodyText);
        startActivity(emailIntent);



    }



}
