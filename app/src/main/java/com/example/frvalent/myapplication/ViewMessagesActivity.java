package com.example.frvalent.myapplication;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.AMQP.Queue.DeclareOk;



import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewMessagesActivity extends AppCompatActivity {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_view_messages);
            Intent intent = getIntent();

            setupConnectionFactory();
            publishToAMQP();
            setupPubButton();

            final Handler incomingMessageHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    String message = msg.getData().getString("msg");
                    TextView tv = (TextView) findViewById(R.id.textView);
                    Date now = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat ("hh:mm:ss");
                    tv.append('\n' + ft.format(now) + ' ' + message + '\n');
                    // tv.setText("test " + message);

                    if (message.toLowerCase().contains("fouine") || message.toLowerCase().contains("weasel")){
                        ImageView iv = (ImageView) findViewById(R.id.weasel);
                        iv.setVisibility(View.VISIBLE);
                    }
                    else if (message.toLowerCase().contains("audio")){

                        ImageView ivspk = (ImageView) findViewById(R.id.speaker);
                        ivspk.setVisibility(View.VISIBLE);
                        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.alert);
                        mediaPlayer.start();

                    }
                    else {
                        ImageView iv = (ImageView) findViewById(R.id.weasel);
                        iv.setVisibility(View.INVISIBLE);
                    }

                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;

                    String toastMessage = "NEW ALERT FROM CERN: \n" + message;
                    Toast toast = Toast.makeText(context, toastMessage, duration);
                    toast.show();
                    toast.setGravity(Gravity.TOP, 0, 0);

                         }
            };
            subscribe(incomingMessageHandler);
        }

        void setupPubButton() {
            Button button = (Button) findViewById(R.id.publish);
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    EditText et = (EditText) findViewById(R.id.text);
                    publishMessage(et.getText().toString());
                    et.setText("");
                }
            });
        }

        Thread subscribeThread;
        Thread publishThread;
        @Override
        protected void onDestroy() {
            super.onDestroy();
            publishThread.interrupt();
            subscribeThread.interrupt();
        }

        private BlockingDeque<String> queue = new LinkedBlockingDeque<String>();
        void publishMessage(String message) {
            //Adds a message to internal blocking queue
            try {
                Log.d("","[q] " + message);
                queue.putLast(message);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        ConnectionFactory factory = new ConnectionFactory();
        private void setupConnectionFactory() {
            String uri = "CLOUDAMQP_URL";
            try {
                factory.setAutomaticRecoveryEnabled(false);
                //factory.setUri("amqp://freeswitch-test.cern.ch");
                factory.setUsername("guest");
                factory.setPassword("guest");
                factory.setVirtualHost("/");
                factory.setHost("freeswitch-test.cern.ch");
                //factory.setPort(5672);
            } catch (/*KeyManagementException | NoSuchAlgorithmException | URISyntaxException e1*/ Exception e1) {
                e1.printStackTrace();
            }
        }

        void subscribe(final Handler handler)
        {
            subscribeThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true) {
                        try {
                            Connection connection = factory.newConnection();
                            Channel channel = connection.createChannel();
                            //channel.basicQos(1);
                            DeclareOk q = channel.queueDeclare("mobile", false, false, false, null);
                            //channel.queueBind(q.getQueue(), "", "mobile");
                            QueueingConsumer consumer = new QueueingConsumer(channel);
                            channel.basicConsume(q.getQueue(), true, consumer);

                            // Process deliveries
                            while (true) {
                                QueueingConsumer.Delivery delivery = consumer.nextDelivery();

                                String message = new String(delivery.getBody());
                                Log.d("","[r] " + message);

                                Message msg = handler.obtainMessage();
                                Bundle bundle = new Bundle();

                                bundle.putString("msg", message);
                                msg.setData(bundle);
                                handler.sendMessage(msg);
                            }
                        } catch (InterruptedException e) {
                            break;
                        } catch (Exception e1) {
                            Log.d("", "Connection broken: " + e1.getClass().getName());
                            try {
                                Thread.sleep(4000); //sleep and then try again
                            } catch (InterruptedException e) {
                                break;
                            }
                        }
                    }
                }
            });
            subscribeThread.start();
        }

        public void publishToAMQP()
        {
            publishThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true) {
                        try {
                            Connection connection = factory.newConnection();
                            Channel ch = connection.createChannel();
                            ch.confirmSelect();

                            while (true) {
                                String message = queue.takeFirst();
                                try{
                                    ch.basicPublish("", "mobile", null, message.getBytes());
                                    //ch.basicPublish("", "chat", null, message.getBytes((Charset.forName("UTF-8"))));
                                    Log.d("", "[s] " + message);
                                    ch.waitForConfirmsOrDie();
                                    /*TextView tv = (TextView) findViewById(R.id.textView);
                                    Date now = new Date();
                                    SimpleDateFormat ft = new SimpleDateFormat ("hh:mm:ss");
                                    tv.append(ft.format(now) + " SENT:" + message + '\n');*/

                                } catch (Exception e){
                                    Log.d("","[f] " + message);
                                    queue.putFirst(message);
                                    throw e;
                                }
                            }
                        } catch (InterruptedException e) {
                            break;
                        } catch (Exception e) {
                            Log.d("", "Connection broken: " + e.getClass().getName());
                            try {
                                Thread.sleep(5000); //sleep and then try again
                            } catch (InterruptedException e1) {
                                break;
                            }
                        }
                    }
                }
            });
            publishThread.start();
        }
    }